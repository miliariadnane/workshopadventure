# GIT

Durant cet atelier, GIT sera votre plus belle plume afin de réaliser votre rêve, **mettre en production votre `sith`**!

## Emplacement de la configuration locale de git

```sh
# Exemple de configuration git.
$ cat $HOME/.gitconfig
# Données associés à l'utilisateur
[user]
	email = <votre email>
	name = <votre nom>
# Strategie de merge par défaut
[pull]
	rebase = true
# Alias git permettant de gagner du temps, par exemple:
# la commande `git checkout` devient `git co`
# la commande `git alias` permet d'afficher tous les alias définis
[alias]
	co = checkout
	ec = config --global -e
	up = !git pull --rebase --prune $@ && git submodule update --init --recursive
	cob = checkout -b
	cm = !git add -A && git commit -m
	save = !git add -A && git commit -m 'SAVEPOINT'
	wip = !git add -u && git commit -m "WIP"
	undo = reset HEAD~1 --mixed
	amend = commit -a --amend --no-edit
	wipe = !git add -A && git commit -qm 'WIPE SAVEPOINT' && git reset HEAD~1 --hard
	# clean merged branchs
	bclean = "!f() { git branch --merged ${1-main} | grep -v " ${1-main}$" | xargs git branch -d; }; f"
	# full repo clean
	bdone = "!f() { git checkout ${1-main} && git up && git bclean ${1-main}; }; f"
	# Clean current repository
	fclean = clean -fx
	# Save current modifications and switch to master
	pause = !git stash --include-untracked && git checkout main && git up
	wks = worktree list
	wka = worktree add
	wkr = worktree remove
	# List branches
	br = branch --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) %(color:green)(%(committerdate:relative)) [%(authorname)]' --sort=-committerdate
	# Show logs
	lg = log --graph --pretty='format:%C(red)%d%C(reset) %C(yellow)%h%C(reset) %ar %C(green)%aN%C(reset) %s'
	authors = shortlog -sn --no-merges
	# List all aliases
	alias = ! git config --get-regexp ^alias\\. | sed -e s/^alias\\.// -e s/\\ /\\ =\\ /
	# Show logs
	loga = log --graph --decorate --name-status --all
	# Search inside history
	se = !git rev-list --all | xargs git grep -F
```

## Avoir de l'aide sur une commande git

```sh
git help <nom de la commande>
```

## Cloner un dépot distant

```sh
git clone <URL du dépot distant> [<Répertoire cible (optionel)>]
```

## Créer une branche en local

```sh
git checkout -b <nom de la branche>
```

## Ajouter une modification sur un fichier

```sh
git add <emplacement du fichier ou dossier>
git commit -m "Message de commit associé à l'ajout"
```

## Ajouter toutes les modification du dépot local

```sh
git add -A
git commit -m "Message de commit associé à l'ajout"
```

## Publier les modifications commités en local sur le dépot distant

```sh
git push
```

## Récupérer les modification du dépot distant en local

```sh
git pull
```

## Restorer un fichier modifier en local avant de l'avoir commité

```sh
git checkout -- <emplacement du fichier>
```

## Ajouter des modifications sur un commit existant en local

```sh
git commit --amend --no-edit
```

## Supprimer toutes les modification faites en local sur la branche courante

```sh
git reset --hard HEAD
```

## Mettre de côté et restaurer les modifications courantes

```sh
# Mettre de côté les modifications
git stash
# Les restaurer
git stash pop
```
