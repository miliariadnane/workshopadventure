## Kustomize dans ArgoCD

Ces `overlays` sont gérés dans les applications tout simplement en utilisant le `spec.source.path`, par exemple

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello-world
  namespace: argocd
spec:
  project: default
  destination:
    namespace: test
    server: https://kubernetes.default.svc
  source:
    path: 02-kustomize/overlays/demo
    repoURL: https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
```

À vous de :

- créer le manifest de l'`Application ArgoCD` pour déployer en **PRODUCTION**
- et modifier la configuration de production pour utiliser la version **1.0** du `sith`.

**Rappels :**

- les apps sont chargés depuis des commandes **Depuis votre workspace gipods `workshopadventure`.**
- les descripteurs de déploiements (changement de la version de l'image de production par exemple) sont à modifier dans le **repository git `deploy-sith-from-gitlab`**
